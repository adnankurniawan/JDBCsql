package com.ank.jdbcmysql;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.zxing.client.android.CaptureActivity;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class MainActivity extends AppCompatActivity {
    Connection mConn;
    EditText et_data;
    Button b_write;
    String mQuery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        allowNetworkOnMainThread();
        mConn = new ConnectDb().getConnection();

        et_data = (EditText) findViewById(R.id.et_data);
        b_write = (Button) findViewById(R.id.b_write);

        b_write.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeData(et_data.getText().toString());
            }
        });

        b_write.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                runtimePermission();
                return true;
            }
        });


    }

    void runtimePermission(){
        int permissionCheck = ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.CAMERA);
        if(permissionCheck== PackageManager.PERMISSION_DENIED){
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA},1998);
            Log.d("Request", "start launch request");
        }else {
            Intent intent = new Intent(getApplicationContext(),CaptureActivity.class);
            intent.setAction("com.google.zxing.client.android.SCAN");
            intent.putExtra("SAVE_HISTORY", false);
            intent.putExtra("TITLE_SCAN","Scan Move Order");
            startActivityForResult(intent, 0);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode==1998 && grantResults[0]==PackageManager.PERMISSION_GRANTED){
            runtimePermission();
        }else{
            Toast.makeText(this, "Izinkan aplikasi mengakses kamera untuk melakukan SCAN", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, final int resultCode, final Intent data) {
        if (requestCode == 0) {
            Log.d("Result", ""+requestCode);
            if (resultCode == RESULT_OK) {
                final String contents = data.getStringExtra("SCAN_RESULT");
                et_data.setText(contents);
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(MainActivity.this, "Canceled", Toast.LENGTH_SHORT).show();
            }
        }
    }

    void writeData(String data){
        try{
            Statement statement = mConn.createStatement();
            mQuery = "INSERT INTO tb_data VALUES('"+data+"')";
            Log.d("Query",mQuery);
            int rowAfect = statement.executeUpdate(mQuery);
            Log.d("UPDATE",rowAfect+" row afected");
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    void allowNetworkOnMainThread(){
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }


}
